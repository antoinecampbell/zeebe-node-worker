import {ZBClient} from 'zeebe-node/dist';

function main() {
    const client = new ZBClient('localhost:26500');
    client.createWorker('node-worker', 'task',
        (job, complete) => {
            console.log(job);
            complete.success();
        });
}

main();