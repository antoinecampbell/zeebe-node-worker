# zeebe-node-worker

## Setup
- Use the zeebe docker compose repo to stand up a zeebe cluster: https://github.com/zeebe-io/zeebe-docker-compose/tree/master/operate
    - From the operate directory start a cluster: `docker-compose up`
- Install node dependencies: `npm install`
- Run the worker: `npm start`
- Deploy the bpmn process to zeebe `zbctl --insecure zeebe-test.bpmn`
- Start a process instance: `zbctl --insecure create instance zeebe-test --variables '{"data": 10}'`
